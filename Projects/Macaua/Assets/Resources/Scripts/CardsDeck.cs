﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CardsDeck : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
	public Canvas canvas;

	public GameObject pOneDeck;
	public GameObject pTwoDeck;

	private bool isMouseClicked;

	private void Start()
	{
		Create();
	}

	public void OnPointerDown(PointerEventData eventData)
	{
		isMouseClicked = true;
		PickCard();
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		isMouseClicked = false;
	}

	private void Create()
	{
		gameObject.transform.SetParent(canvas.transform);

		gameObject.transform.localScale = new Vector3(2.5f, 2.5f, 2.5f);

		gameObject.transform.position = new Vector2(Screen.width - Screen.width / 10, Screen.height / 2);
	}

	private void PickCard()
	{
		GameObject lastCard = GlobalVariables.cardsDeck[GlobalVariables.cardsDeck.Count - 1];

		GameObject newCard = Instantiate(lastCard);

		newCard.AddComponent<MouseClickEvent>();

		newCard.GetComponent<MouseClickEvent>().isNewCard = true;

		newCard.GetComponent<MouseClickEvent>().player = GlobalVariables.turn;

		newCard.GetComponent<MouseClickEvent>().isMouseClicked = true;

		GlobalVariables.cardsInHand.Add(newCard);
		GlobalVariables.cardsDeck.Remove(lastCard);


	}
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Cards : MonoBehaviour
{
	public Canvas canvas;

	public GameObject pOneDeck;
	public GameObject pTwoDeck;

	private Vector3 offset = new Vector3(-(Screen.width / 48), Screen.height / 50, 0f);

	private void Start()
	{
		CreateCardsDeck();
		RandomizeCards();
		ShowCardsPlayerOne();
		ShowCardsPlayerTwo();
		CreateLastCard();
	}

	private void Update()
	{
		MoveCards();
		SetParent();
	}

	private void CreateCardsDeck()
	{
		//Important note: place your prefabs folder(or levels or whatever) 
		//in a folder called "Resources" like this "Assets/Resources/Prefabs"
		Object[] subListObjects = Resources.LoadAll("Prefabs/Carti", typeof(GameObject));
		//This may be sloppy (I've only been programing for a short time) 
		//It works though :) 
		foreach (GameObject subListObject in subListObjects)
		{
			GameObject lo = (GameObject)subListObject;
			GlobalVariables.cardsDeck.Add(lo);
		}
	}

	private void ShowCardsPlayerOne()
	{
		// The y position for evey card.
		float y = Screen.height / 7;

		// Distance between cards.
		int offset = Screen.width / 10;

		// Middle card
		float x3 = Screen.width / 2;

		// Left cards relative to the middle one.
		float x1 = x3 - offset * 2;
		float x2 = x3 - offset * 1;

		// Right cards relative to the middle one.
		float x4 = x3 + offset * 1;
		float x5 = x3 + offset * 2;

		int i;

		for (i = 0; i < 5; i++)
		{
			// The integer randomValue it gets a random value that it does represent what card from the deck to pick randomly.
			int randomValue = Random.Range(0, GlobalVariables.cardsDeck.Count - 1);

			// The GameObject card is now the spawned card on the scene, that is picked randomly from the deck.
			GameObject card = Instantiate(GlobalVariables.cardsDeck[randomValue]);

			// It adds the MouseClickEvent script to all the cards.
			card.AddComponent<MouseClickEvent>();

			// It adds the card to the player one deck list.
			GlobalVariables.playerOneDeck.Add(card);

			// It removes the card from the initial cards deck.
			GlobalVariables.cardsDeck.RemoveAt(randomValue);

			// It does set the parent of the card.
			card.transform.SetParent(pOneDeck.transform);

			// Makes the scale of the card to be 1.5.
			card.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);

			// It puts the cards in place.
			switch (i)
			{
				case 0:
					card.transform.position = new Vector2(x1, y);
					break;
				case 1:
					card.transform.position = new Vector2(x2, y);
					break;
				case 2:
					card.transform.position = new Vector2(x3, y);
					break;
				case 3:
					card.transform.position = new Vector2(x4, y);
					break;
				case 4:
					card.transform.position = new Vector2(x5, y);
					break;
			}
		}
	}

	// Same as above.
	private void ShowCardsPlayerTwo()
	{
		float y = Screen.height - Screen.height / 7;

		int offset = Screen.width / 10;

		float x3 = Screen.width / 2;

		float x1 = x3 - offset * 2;
		float x2 = x3 - offset * 1;

		float x4 = x3 + offset * 1;
		float x5 = x3 + offset * 2;

		int i = 0;

		for (i = 0; i < 5; i++)
		{
			int randomValue = Random.Range(0, GlobalVariables.cardsDeck.Count - 1);

			GameObject cardTemporary = GlobalVariables.cardsDeck[randomValue];

			GameObject card = Instantiate(cardTemporary);

			card.AddComponent<MouseClickEvent>();

			GlobalVariables.playerTwoDeck.Add(card);

			GlobalVariables.cardsDeck.RemoveAt(randomValue);

			card.transform.SetParent(pTwoDeck.transform);

			card.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);

			switch (i)
			{
				case 0:
					card.transform.position = new Vector2(x1, y);
					break;
				case 1:
					card.transform.position = new Vector2(x2, y);
					break;
				case 2:
					card.transform.position = new Vector2(x3, y);
					break;
				case 3:
					card.transform.position = new Vector2(x4, y);
					break;
				case 4:
					card.transform.position = new Vector2(x5, y);
					break;
			}
		}
	}

	private void MoveCards()
	{
		for (int i = 0; i < GlobalVariables.cardsInHand.Count; i++)
		{
			if (GlobalVariables.cardsInHand[i].GetComponent<MouseClickEvent>().isMouseClicked)
			{
				GlobalVariables.cardsInHand[i].GetComponent<Transform>().position = Input.mousePosition;
				GlobalVariables.cardsInHand[i].GetComponent<Transform>().position += offset;
			}
		}
	}

	private void SetParent()
	{
		for (int i = 0; i < GlobalVariables.cardsInHand.Count; i++)
		{
			GlobalVariables.cardsInHand[i].transform.SetParent(GameObject.Find("Cards In Hand").transform);
		}
		for (int i = 0; i < GlobalVariables.playerOneDeck.Count; i++)
		{
			GlobalVariables.playerOneDeck[i].transform.SetParent(GameObject.Find("Player One Deck").transform);
		}
		for (int i = 0; i < GlobalVariables.playerTwoDeck.Count; i++)
		{
			GlobalVariables.playerTwoDeck[i].transform.SetParent(GameObject.Find("Player Two Deck").transform);
		}
	}

	private void RandomizeCards()
	{

		for (int i = 0; i < GlobalVariables.cardsDeck.Count; i++)
		{
			GameObject temp = GlobalVariables.cardsDeck[i];
			int randomIndex = Random.Range(0, GlobalVariables.cardsDeck.Count);
			GlobalVariables.cardsDeck[i] = GlobalVariables.cardsDeck[randomIndex];
			GlobalVariables.cardsDeck[randomIndex] = temp;
		}
	}

	private void CreateLastCard()
	{
		print(GlobalVariables.cardsDeck[GlobalVariables.cardsDeck.Count - 1]);

		GameObject lastCard = GlobalVariables.cardsDeck[GlobalVariables.cardsDeck.Count - 1];
		GameObject card = Instantiate(lastCard);


		card.transform.SetParent(GameObject.Find("DRAW").transform);
		card.transform.position = new Vector2(Screen.width / 10, Screen.height / 2);
		card.transform.localScale = new Vector3(2.5f, 2.5f, 2.5f);
	}
}
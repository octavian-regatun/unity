﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalVariables : MonoBehaviour
{
	public static int turn = 1;

	public static List<GameObject> cardsInHand = new List<GameObject>();
	public static List<GameObject> cardsDeck = new List<GameObject>();
	public static List<GameObject> playerOneDeck = new List<GameObject>();
	public static List<GameObject> playerTwoDeck = new List<GameObject>();
	public static List<GameObject> cardsDown = new List<GameObject>();
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Border : MonoBehaviour
{
	public Canvas canvas;
	public GameObject borderTemp;
	public GameObject borders;

	private void Start()
	{
		ShowBorderPlayerOne();
		ShowBorderPlayerTwo();
	}

	void ShowBorderPlayerOne()
	{
		// It spawns the border (black line) as a game object on the scene.
		GameObject border = Instantiate(borderTemp);

		// It does set the parent of the object to borders.
		border.transform.SetParent(borders.transform);

		// Makes the scale image of the object to be 1.
		border.transform.localScale = new Vector3(1f, 1f, 1f);

		// It puts the border in the right place where it should be on the screen (relative to the screen resolution - width, height).
		border.transform.position = new Vector2(Screen.width / 2, Screen.height - Screen.height / 10 * 3);
	}

	// Same as above.
	void ShowBorderPlayerTwo()
	{
		GameObject border = Instantiate(borderTemp);

		border.transform.SetParent(borders.transform);

		border.transform.localScale = new Vector3(1f, 1f, 1f);

		// Just another position.
		border.transform.position = new Vector2(Screen.width / 2, Screen.height / 10 * 3);
	}
}

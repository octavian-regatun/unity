﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

public class MouseClickEvent : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
	public int player;

	private GameObject pOneDeck;
	private GameObject pTwoDeck;

	// Bool that keeps track if the mouse is clicked or not.
	public bool isMouseClicked;

	private Vector2 initialPosition;

	private Vector2 lastPosition;

	public bool isNewCard = false;

	private int count = 0;

	// Initialize variables.
	private void Start()
	{
		// Store the initial position for every card in a Vector2 (x, y).


		pOneDeck = GameObject.Find("Player One Deck");
		pTwoDeck = GameObject.Find("Player Two Deck");

		GetInitialPosition();
		GetPlayer();
		DefaultValues();
	}

	public void OnPointerDown(PointerEventData eventData)
	{
		isMouseClicked = true;

		TakeCardInHand();
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		isMouseClicked = false;

		PutCardOnTable();
		GetLastPosition();
		NewCardInitialPosition();
		PutToRightPlace();
	}

	private void DefaultValues()
	{
		gameObject.transform.localScale = new Vector3(2f, 2f, 2f);
	}

	private void GetInitialPosition()
	{
		if (isNewCard == false)
		{
			initialPosition = gameObject.transform.position;
		}
	}

	private void TakeCardInHand()
	{
		// If that card isn't already in that list that keeps track of the cards that are in hand, it adds that card.
		GlobalVariables.cardsInHand.Add(gameObject);

		// Verify to what player the card belongs to.
		if (player == 1)
		{
			// Remove the card from the list that keeps track of the cards from the specific player deck.
			GlobalVariables.playerOneDeck.Remove(gameObject);
		}
		// Same as above.
		else if (player == 2)
		{
			GlobalVariables.playerTwoDeck.Remove(gameObject);
		}
	}

	private void PutCardOnTable()
	{
		// When you let the card down on the table (release the click) it removes from the list that keeps track of the cards in hand.
		GlobalVariables.cardsInHand.Remove(gameObject);
		if (player == 1)
		{

			GlobalVariables.playerOneDeck.Add(gameObject);
		}
		else if (player == 2)
		{

			GlobalVariables.playerTwoDeck.Add(gameObject);
		}
	}

	private void GetLastPosition()
	{
		lastPosition = gameObject.transform.position;
	}

	private void PutToRightPlace()
	{

		if (player == 1)
			if (lastPosition.y > Screen.height / 10 * 3)
			{
				gameObject.transform.position = initialPosition;
			}
		if (player == 2)
		{
			if (lastPosition.y < Screen.height - Screen.height / 10 * 3)
			{
				gameObject.transform.position = initialPosition;
			}
		}

	}

	private void GetPlayer()
	{
		// In case if you pick a card, so you don't get an error.
		if (player == 0)
		{
			if (Utilities.GetParent(gameObject) == pOneDeck)
			{
				player = 1;
			}
			else if (Utilities.GetParent(gameObject) == pTwoDeck)
			{
				player = 2;
			}
		}
	}

	private void NewCardInitialPosition()
	{
		if (isNewCard)
		{
			if (count == 0)
			{
				if (player == 1)
				{
					if (lastPosition.y < Screen.height / 10 * 3)
					{
						print("test");
						initialPosition = gameObject.transform.position;
						count++;
					}
				}
				else if (player == 2)
				{
					if (lastPosition.y < Screen.height - Screen.height / 10 * 3)
					{
						initialPosition = gameObject.transform.position;
						count++;
					}
				}
			}
		}
	}
}
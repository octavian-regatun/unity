﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utilities : MonoBehaviour {

	// This function it does get the parent transform of the object specified as a parameter.
	public static GameObject GetParent(GameObject @object)
	{
		return @object.transform.parent.gameObject;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RemainingCardsText : MonoBehaviour
{
	public Text text;

	private void Update()
	{
		text.text = "Remaining Cards: " + GlobalVariables.cardsDeck.Count;
	}
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoneyAlgorithm : MonoBehaviour
{
	private bool adaugaSauScade;

	public static float chance = 0.5f;

	private void Start()
	{
		StartCoroutine(Delay(0.5f));
		StartCoroutine(Delay1(8f));
	}

	public bool RandomBoolean(float _chance)
	{
		if (Random.value >= _chance)
		{
			return true;
		}

		else
		{
			return false;
		}
	}

	private void TrendsValue()
	{
		adaugaSauScade = RandomBoolean(chance);
		if (adaugaSauScade == true)
		{
			GlobalVariables.trends += GlobalVariables.trendsRandomValue;
		}
		else
		{
			GlobalVariables.trends -= GlobalVariables.trendsRandomValue;
		}
	}

	private void TrendsLimitValue()
	{
		if (GlobalVariables.trends > 25000)
		{
			chance = 0.75f;
		}
		else if (GlobalVariables.trends < 300)
		{
			chance = 0.1f;
			GlobalVariables.trends += 200;
		}
	}

	private void TrendsRandomValue()
	{
		bool adaugaSauScade = RandomBoolean(0.5f);

		if (adaugaSauScade == true)
		{
			GlobalVariables.trendsRandomValue += Random.Range(0, 50);
		}
		else
		{
			GlobalVariables.trendsRandomValue -= Random.Range(0, 50);
		}
	}

	private void TrendsLimitRandomValue()
	{
		if (GlobalVariables.trendsRandomValue > 500)
		{
			GlobalVariables.trendsRandomValue -= Random.Range(100, 200);
		}
		else if (GlobalVariables.trendsRandomValue < 0)
		{
			GlobalVariables.trendsRandomValue += Random.Range(100, 200);
		}
	}

	private void ChanceMutate()
	{
		if (GlobalVariables.trends < 25000 && GlobalVariables.trends > 250)
		{
			chance = Random.Range(0.3f, 0.7f);
		}
	}

	private void ConsoleLog()
	{
		print("Bitcoin Random Value: " + GlobalVariables.trendsRandomValue + "\n" + "Chance: " + chance);
	}

	private IEnumerator Delay(float waitTime)
	{
		while (true)
		{
			yield return new WaitForSeconds(waitTime);

			TrendsValue();
			TrendsLimitValue();
			TrendsRandomValue();
			TrendsLimitRandomValue();

			ConsoleLog();
		}
	}

	private IEnumerator Delay1(float waitTime)
	{
		while (true)
		{
			yield return new WaitForSeconds(waitTime);
			ChanceMutate();
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoneyClickEvent : MonoBehaviour
{
	public void AddMoneyPerClick()
	{
		GlobalVariables.bitcoin += GlobalVariables.bitcoinPerClick;
	}
}
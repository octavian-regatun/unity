﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TrendArrow : MonoBehaviour {

	public Sprite arrowUp;
	public Sprite arrowDown;

	public Image arrow;

	void Update()
	{
		if (MoneyAlgorithm.chance <= 0.5)
		{
			arrow.sprite = arrowUp;
		}
		else
		{
			arrow.sprite = arrowDown;
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateText : MonoBehaviour
{
	public Text bani;
	public Text bitcoin;
	public Text trends;
	public Text mining;

	void Update()
	{
		bani.text = GlobalVariables.bani.ToString();
		bitcoin.text = GlobalVariables.bitcoin.ToString();
		trends.text = GlobalVariables.trends.ToString();
		mining.text = GlobalVariables.mining.ToString()+ "/min";
	}
}
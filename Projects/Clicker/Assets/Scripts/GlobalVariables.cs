﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalVariables : MonoBehaviour
{
	public static int bani = 0;

	public static decimal bitcoin = 0.0000m;
	public static decimal bitcoinPerClick = 0.0001m;

	public static int trends = 8721;
	public static int trendsRandomValue = 200;

	public static int mining = 0;
}